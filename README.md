#cucumber JVM Test
#===========================

#Applicants are politely asked not to disseminate or discuss the details of this coding test to others.

#Cucumber JVM Test

#There currently is one feature file which requires automating, the search.feature file has been written already, the user story is contained within the feature file and the aim of the task is to automate the remainder of the scenario using cucumber JVM.
#	Please complete the automation of the search scenario and get as far as you can within 2 hour.
#	We are interested in a fully working automated scenario that demonstrates your ability to automate a scenario and actively contribute towards an existing automated test suite, the quality of your solution is as important as how far through the test you get.
#	Your code should be as if you were contributing towards an existing automation test suite in a 'professional' environment. 

#TASKS

#1.	Make an account on bitbucket (its free) and FORK the bitbucket Cucumber-jvm-framework repository to make your own copy on which you will implement your solution.

#2.	Complete the missing step definitions and add any necessary Java code. 

#3.	You should aim to get all steps passing and validating that the story passes.

#4.	Finally, give us (thomsonreuters) access to your fork of the cucumber JVM automation solution (using Administration >> Access Management) so that we can review your solutions.