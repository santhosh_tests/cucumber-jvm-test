package test.java.com.thomsonreuters.grc.accelus.regint.bdd.StepDefinitions;


import javax.naming.directory.SearchResult;

import org.jboss.netty.handler.codec.oneone.OneToOneStrictEncoder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.thomsonreuters.grc.accelus.regint.bdd.pageobjects.Search;
import com.thomsonreuters.grc.accelus.regint.bdd.pageobjects.SearchResults;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class SearchStepDef {

	Search onSearchPage;
	SearchResults onSearchResultPage;
	WebDriver driver = new FirefoxDriver();
	
    @Given("^I am on the google search page$")
    public void navigateToSearch() throws Throwable {
    	onSearchPage = new Search(driver);
    	onSearchPage.navigateToSearchPage();
    }

    @When("^I search for the term \"([^\"]*)\"$")
    public void I_search_for_the_term(String search_term) throws Throwable {
    	onSearchPage.enterSearchText();
    	onSearchPage.clickSearchButton();
    }

    @Then("^the Thomson Reuters company website link is returned within the results$")
    public void the_Thomson_Reuters_company_website_link_is_returned_within_the_results() throws Throwable {
    	onSearchResultPage = new SearchResults(driver);
    	onSearchResultPage.checkThomsonReutersSiteDisplayed();
    }

}
