package com.thomsonreuters.grc.accelus.regint.bdd.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import test.java.com.thomsonreuters.grc.accelus.regint.bdd.support.Driver;


public class SearchResults extends Driver 
{
	
	public SearchResults(WebDriver driver) {
		super(driver);
	}

	public SearchResults checkThomsonReutersSiteDisplayed()
	{
		Assert.assertTrue(driver.getPageSource().contains("thomsonreuters.com"));		
		return new SearchResults(driver);
	}

	
}
