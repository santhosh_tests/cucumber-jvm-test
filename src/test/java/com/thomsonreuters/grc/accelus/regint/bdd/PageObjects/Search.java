package com.thomsonreuters.grc.accelus.regint.bdd.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import test.java.com.thomsonreuters.grc.accelus.regint.bdd.support.*;

public class Search extends Driver{

	public Search(WebDriver driver)
	{
		super(driver);
	}
	
    public Search navigateToSearchPage(){
    	
    	driver.navigate().to("http://www.google.com");
		return new Search(driver);
    }
		
	public Search enterSearchText()
	{
		driver.findElement(By.name("q")).sendKeys("Thomson Reuters");
		return new Search(driver);
	}
	
	public SearchResults clickSearchButton()
	{
		driver.findElement(By.name("btnK")).click();
		return new SearchResults(driver);
	}
	
	
	
}
